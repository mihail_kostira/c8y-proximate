package com.softwareag.c8y.example.proximate;

public class DeviceCredentials {
	
	protected String username;
	protected String password;
	
	public DeviceCredentials(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	 
}
