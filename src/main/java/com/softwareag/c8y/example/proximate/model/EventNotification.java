package com.softwareag.c8y.example.proximate.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "realtimeAction", "data" })
public class EventNotification {

	@JsonProperty("realtimeAction")
	private String realtimeAction;
	@JsonProperty("data")
	private LocationEvent locationEvent;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("realtimeAction")
	public String getRealtimeAction() {
		return realtimeAction;
	}

	@JsonProperty("realtimeAction")
	public void setRealtimeAction(String realtimeAction) {
		this.realtimeAction = realtimeAction;
	}


	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public LocationEvent getLocationEvent() {
		return locationEvent;
	}

	public void setLocationEvent(LocationEvent locationEvent) {
		this.locationEvent = locationEvent;
	}

}
