package com.softwareag.c8y.example.proximate;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.cumulocity.microservice.context.ContextService;
import com.cumulocity.microservice.context.ContextServiceImpl;
import com.cumulocity.microservice.context.credentials.MicroserviceCredentials;
import com.cumulocity.microservice.subscription.model.MicroserviceSubscriptionAddedEvent;
import com.cumulocity.sdk.client.Platform;
import com.cumulocity.sdk.client.PlatformParameters;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import com.cumulocity.sdk.client.measurement.MeasurementCollection;
import com.cumulocity.sdk.client.measurement.PagedMeasurementCollectionRepresentation;
import com.cumulocity.sdk.client.notification.Subscriber;
import com.cumulocity.sdk.client.notification.SubscriberBuilder;
import com.cumulocity.sdk.client.notification.Subscription;
import com.cumulocity.sdk.client.notification.SubscriptionListener;
import com.cumulocity.sdk.client.notification.SubscriptionNameResolver;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softwareag.c8y.example.proximate.model.C8yPosition;
import com.softwareag.c8y.example.proximate.model.EventNotification;
import com.softwareag.c8y.example.proximate.model.LocationEvent;

/**
 *
 */
@Component
public class LocationListener implements SubscriptionListener<String, Object> {

	private static final int MAX_STORED_EVENTS_PER_DEVICE = 100;

	@Autowired
	private Platform tenantPlatform;

	@Autowired
	private ContextService<MicroserviceCredentials> microserviceContextService;

	@Autowired
	private MeasurementApi measurementApi;
		
	private MicroserviceCredentials credentials;

	private HashMap<String, Deque<EventNotification>> deviceLocationUpdates;
	private HashMap<String, List<String>> subscribedDevices;
	
	public LocationListener() {
		deviceLocationUpdates = new HashMap<String, Deque<EventNotification>>();
		subscribedDevices = new HashMap<String, List<String>>();
	}

	@EventListener
	public void handleEvent(Object event) {
		// This event is fired after we have used out bootstrap credentials to get
		// our service credentials(the credentials we use to talk to the tenant).
		// It is fired once for each tenant that is subscribed to this microservice
		if (event instanceof MicroserviceSubscriptionAddedEvent) {
			MicroserviceSubscriptionAddedEvent subscriptionAddedEvent = (MicroserviceSubscriptionAddedEvent) event;
			System.out.println("credentials: " + subscriptionAddedEvent.getCredentials());

			credentials = subscriptionAddedEvent.getCredentials();

			setupNotifications();
		}
		System.out.println("event: " + event);
	}

	private void setupNotifications() {
		String channelId = "/events/*";

		Subscriber<String, Object> subscriber = SubscriberBuilder.<String, Object>anSubscriber()
				.withParameters((PlatformParameters) tenantPlatform).withEndpoint("cep/realtime")
				.withSubscriptionNameResolver(new Identity()).withDataType(Object.class).build();

		System.out.println("Subscribing to channel: " + channelId);
		Subscription<String> subscription = subscriber.subscribe(channelId, this);
	}

	private static final class Identity implements SubscriptionNameResolver<String> {
		@Override
		public String apply(String id) {
			return id;
		}
	}

	@Override
	public void onNotification(Subscription<String> subscription, Object notification) {
		//System.out.println("Received notification: " + notification);

		HashMap<String, Object> hashMap = (HashMap<String, Object>) notification;

		if (hashMap.get("realtimeAction").equals("CREATE")
				&& ((HashMap<String, Object>) hashMap.get("data")).containsKey("c8y_Position")) {

			ObjectMapper mapper = new ObjectMapper();
			EventNotification eventNotification = mapper.convertValue(notification, EventNotification.class);

			// add device if not added
			String deviceId = eventNotification.getLocationEvent().getSource().getId();
			if (!deviceLocationUpdates.containsKey(deviceId)) {
				// subscribe all other devices to this device and vice versa
				List<String> subscribed = new ArrayList<String>();
				for (String otherId : deviceLocationUpdates.keySet()) {
					// subscribe other to us
					subscribed.add(otherId);
					// subscribe to other
					subscribedDevices.get(otherId).add(deviceId);
				}
				subscribedDevices.put(deviceId, subscribed);
				deviceLocationUpdates.put(deviceId, new LinkedList<EventNotification>());
				
			}

			// add update to history
			Deque<EventNotification> eventNotifications = deviceLocationUpdates.get(deviceId);
			eventNotifications.addLast(eventNotification);
			if (eventNotifications.size() > MAX_STORED_EVENTS_PER_DEVICE) {
				eventNotifications.removeFirst();
			}

			processLocationUpdate(deviceId);

			//System.out.println("Received notification: " + eventNotification.getLocationEvent().getSource().getName());
		}
	}

	private void processLocationUpdate(String deviceId) {
		// notify all subscribed devices if within treshold
		LocationEvent locationEvent = deviceLocationUpdates.get(deviceId).getLast().getLocationEvent();
		List<String> subscribers = subscribedDevices.get(deviceId);
		for (String subscriber : subscribers) {
			LocationEvent subscriberLocationEvent = deviceLocationUpdates.get(subscriber).getLast().getLocationEvent();
			double distance = distance(locationEvent.getC8yPosition(), subscriberLocationEvent.getC8yPosition());
			System.out.println(String.format("The distance between device %s and device %s is now %f", deviceId, subscriber, distance));
		}
	}

	public String getStatus() {
		StringBuilder status = new StringBuilder(); 
		Set<String> keySet = subscribedDevices.keySet();
		for (Iterator iterator = keySet.iterator(); iterator.hasNext();) {
			String deviceId = (String) iterator.next();
			List<String> subscribers = subscribedDevices.get(deviceId);
			for (String subscriber : subscribers) {
				LocationEvent subscriberLocationEvent = deviceLocationUpdates.get(subscriber).getLast().getLocationEvent();
				LocationEvent locationEvent = deviceLocationUpdates.get(deviceId).getLast().getLocationEvent();
				double distance = distance(locationEvent.getC8yPosition(), subscriberLocationEvent.getC8yPosition());
				status.append(String.format("The distance between device %s and device %s is now %f", deviceId, subscriber, distance));
			}
		}
		return status.toString();
	}

	private double distance(C8yPosition pos1, C8yPosition pos2) {
		double lat1 = pos1.getLat();
		double lat2 = pos2.getLat();
		double lon1 = pos1.getLng();
		double lon2 = pos2.getLng();
		double el1 = 0;//pos1.getAlt();
		double el2 = 0;//pos2.getAlt();

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}		

	@Override
	public void onError(Subscription<String> subscription, Throwable ex) {
		System.out.println("Subscription error: " + ex);
	}
}
