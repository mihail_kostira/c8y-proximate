package com.softwareag.c8y.example.proximate.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "creationTime", "c8y_Position", "self", "time", "id", "source", "text", "type" })
public class LocationEvent {

	@JsonProperty("creationTime")
	private String creationTime;
	@JsonProperty("c8y_Position")
	private C8yPosition c8yPosition;
	@JsonProperty("self")
	private String self;
	@JsonProperty("time")
	private String time;
	@JsonProperty("id")
	private String id;
	@JsonProperty("source")
	private Source source;
	@JsonProperty("text")
	private String text;
	@JsonProperty("type")
	private String type;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("creationTime")
	public String getCreationTime() {
		return creationTime;
	}

	@JsonProperty("creationTime")
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	@JsonProperty("c8y_Position")
	public C8yPosition getC8yPosition() {
		return c8yPosition;
	}

	@JsonProperty("c8y_Position")
	public void setC8yPosition(C8yPosition c8yPosition) {
		this.c8yPosition = c8yPosition;
	}

	@JsonProperty("self")
	public String getSelf() {
		return self;
	}

	@JsonProperty("self")
	public void setSelf(String self) {
		this.self = self;
	}

	@JsonProperty("time")
	public String getTime() {
		return time;
	}

	@JsonProperty("time")
	public void setTime(String time) {
		this.time = time;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("source")
	public Source getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(Source source) {
		this.source = source;
	}

	@JsonProperty("text")
	public String getText() {
		return text;
	}

	@JsonProperty("text")
	public void setText(String text) {
		this.text = text;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}