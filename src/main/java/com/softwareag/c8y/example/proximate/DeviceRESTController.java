package com.softwareag.c8y.example.proximate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@PreAuthorize("permitAll()")
public class DeviceRESTController {

	private DeviceService deviceService;
	
	@Autowired
	private LocationListener locationListener;
	
	
	@Autowired
	public DeviceRESTController(DeviceService deviceService) {
		this.deviceService = deviceService;
	}
	
	@PreAuthorize("permitAll()")
	@PostMapping(path="/device/IMEI/{IMEI}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DeviceCredentials> createDevice(@PathVariable String IMEI) throws Exception {
		DeviceCredentials deviceCredentials = deviceService.createDeviceCredentials(IMEI);
		return new ResponseEntity<DeviceCredentials>(deviceCredentials, HttpStatus.OK);
	}
	
	@PreAuthorize("permitAll()")
	@GetMapping(path="/device/IMEI/{IMEI}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DeviceCredentials> getDevice(@PathVariable String IMEI) throws Exception {
		DeviceCredentials deviceCredentials = deviceService.createDeviceCredentials(IMEI);
		return new ResponseEntity<DeviceCredentials>(deviceCredentials, HttpStatus.OK);
	}

	@GetMapping(path = "/status", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> status() {
		String status = locationListener.getStatus();
        return new ResponseEntity<String>(status, HttpStatus.OK);
    }

}
