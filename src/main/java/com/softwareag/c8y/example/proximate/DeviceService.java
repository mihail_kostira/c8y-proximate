package com.softwareag.c8y.example.proximate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cumulocity.microservice.context.ContextService;
import com.cumulocity.microservice.context.credentials.MicroserviceCredentials;
import com.cumulocity.microservice.context.inject.TenantScope;
import com.cumulocity.microservice.subscription.model.MicroserviceSubscriptionAddedEvent;
import com.cumulocity.model.authentication.CumulocityCredentials;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.AbstractExtensibleRepresentation;
import com.cumulocity.rest.representation.ResourceRepresentation;
import com.cumulocity.rest.representation.devicebootstrap.NewDeviceRequestRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation;
import com.cumulocity.rest.representation.operation.DeviceControlMediaType;
import com.cumulocity.sdk.client.Platform;
import com.cumulocity.sdk.client.PlatformImpl;
import com.cumulocity.sdk.client.PlatformParameters;
import com.cumulocity.sdk.client.RestConnector;
import com.cumulocity.sdk.client.SDKException;
import com.cumulocity.sdk.client.devicecontrol.DeviceCredentialsApi;
import com.cumulocity.sdk.client.inventory.InventoryApi;

@Service
public class DeviceService {
		
	public static final String DEVICE_REQUEST_URI = "devicecontrol/newDeviceRequests";
	public static final String DEVICE_CREDENTIALS_URI = "devicecontrol/deviceCredentials";

	@Autowired
	private InventoryApi inventoryApi;

	private String deviceBootstrapHost = "https://management.cumulocity.com";
	private String deviceBootstrapUsername = "management/devicebootstrap";
	private String deviceBootstrapPassword = "Fhdt1bb1f";


	@Autowired
	private RestConnector tenantRestConnector;

	private RestConnector deviceBootstrapRestConnector;
	private boolean subscribedToTennant;

	public DeviceService() {
		CumulocityCredentials deviceBootstrapCredentials = new CumulocityCredentials(deviceBootstrapUsername,
				deviceBootstrapPassword);
		// TODO close platformParameters
		PlatformParameters platformParameters = new PlatformImpl(deviceBootstrapHost, deviceBootstrapCredentials);
		deviceBootstrapRestConnector = platformParameters.createRestConnector();

	}

	@EventListener
	public void handleEvent(Object event) {
		if (event instanceof MicroserviceSubscriptionAddedEvent) {
			subscribedToTennant = true;
		}
	}

	public DeviceCredentials createDeviceCredentials(String IMEI) throws Exception {
		if(!subscribedToTennant) {
			throw new Exception("not registered/subscribed to tennant!");
		}
		
		AbstractExtensibleRepresentation newDeviceRequest = getNewDeviceRequest(IMEI);
		if (newDeviceRequest == null) {
			newDeviceRequest = createNewDeviceRequest(IMEI);
		}
		String status = (String) newDeviceRequest.getProperty("status");
		if (!status.equals("WAITING_FOR_CONNECTION")) {
			throw new Exception(
					String.format("Unexpected newDeviceRequest status for device id %s : %s", IMEI, status));
		}

		DeviceCredentials deviceCredentials = null;
		// the first poll changes status from WAITING_FOR_CONNECTION to pending
		// acceptance
		pollCredentials(IMEI, true);
		acceptDeviceRequest(IMEI); // status changes to accepted, now we can get credentials
		deviceCredentials = pollCredentials(IMEI, false);
		return deviceCredentials;
	}

	private AbstractExtensibleRepresentation getNewDeviceRequest(String IMEI) {
		AbstractExtensibleRepresentation representation = null;
		try {
			String requestUrl = tenantRestConnector.getPlatformParameters().getHost() + DEVICE_REQUEST_URI + '/' + IMEI;
			representation = tenantRestConnector.get(requestUrl, DeviceControlMediaType.APPLICATION_JSON_TYPE,
					AbstractExtensibleRepresentation.class);
		} catch (SDKException e) {
			if (e.getMessage().contains("Could not find newDeviceRequest")) {
				//if the newDeviceRequest is not found we get a 500 error, ignore it and return null
			} else {
				System.err.println(String.format("Error getting newDeviceRequest for device id %s : ", IMEI) + e);
				throw e;
			}
		}
		return representation;
	}

	private boolean acceptDeviceRequest(String IMEI) {
		AbstractExtensibleRepresentation representation = null;
		try {
			representation = new AbstractExtensibleRepresentation();
			representation.set("ACCEPTED", "status");
			String requestUrl = tenantRestConnector.getPlatformParameters().getHost() + DEVICE_REQUEST_URI + '/' + IMEI;
			representation = tenantRestConnector.put(requestUrl, DeviceControlMediaType.APPLICATION_JSON_TYPE,
					representation);
		} catch (SDKException e) {
			return false;
		}
		return true;
	}

	/**
	 * @param IMEI
	 * @return true if successful
	 */
	public AbstractExtensibleRepresentation createNewDeviceRequest(String IMEI) {
		AbstractExtensibleRepresentation representation = null;
		try {
			representation = new AbstractExtensibleRepresentation();
			representation.set(IMEI, "id");
			String requestUrl = tenantRestConnector.getPlatformParameters().getHost() + DEVICE_REQUEST_URI;
			representation = tenantRestConnector.post(requestUrl, DeviceControlMediaType.APPLICATION_JSON_TYPE, representation);;
		} catch (SDKException e) {
			System.err.println("error creating creds" + e);
		}
		return representation;
	}

	private DeviceCredentials pollCredentials(String IMEI, boolean ignoreNotFoundException) {
		// DeviceCredentialsApi is broken, so we're doing it manually
		AbstractExtensibleRepresentation representation = null;
		try {
			representation = new AbstractExtensibleRepresentation();
			representation.set(IMEI, "id");
			String requestUrl = deviceBootstrapRestConnector.getPlatformParameters().getHost() + DEVICE_CREDENTIALS_URI;
			representation = deviceBootstrapRestConnector.post(requestUrl, DeviceControlMediaType.APPLICATION_JSON_TYPE,
					representation);
			return new DeviceCredentials((String) representation.getProperty("username"),
					(String) representation.getProperty("password"));
		} catch (SDKException e) {
			if (ignoreNotFoundException
					&& e.getMessage().contains("404")) {
				System.err.println(String.format("Polling credentials for %s: ignoring not found error: ", IMEI, e.toString()));
			} else {
				System.err.println(String.format("Unexpected error polling credentials for %s: ", IMEI, e.toString()));
				throw e;
			}
		}
		return null;
	}
}	

